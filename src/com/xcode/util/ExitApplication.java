package com.xcode.util;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;

public class ExitApplication extends Application
{

	private List<Activity> activityList = new LinkedList<Activity>();

	private static ExitApplication instance;

	private ExitApplication()
	{
	}

	/**
	 * 获取唯一的ExitApplication 实例
	 * @return
	 */
	public static ExitApplication getInstance()
	{
		if (null == instance)
		{
			instance = new ExitApplication();
		}
		return instance;
	}
	/**
	 * 添加acitvity至activity栈中
	 * @param activity	
	 */
	public void addActivity(Activity activity)
	{
		activityList.add(activity);
	}
	/**
	 * 遍历所有activity 并逐个清理结束
	 */
	public void exit()
	{
		for (Activity activity : activityList)
		{
			if ( !activity.isFinishing())
			{
				activity.finish();
			}
		}
		System.exit(0);
	}
}
