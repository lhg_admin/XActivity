package com.xcode.util;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;

public class AndroidUtil
{
	public static class Application
	{
		/**
		 * 安装应用程序的方法
		 * @param context	上下文
		 * @param path		应用程序的路径
		 */
		public static void installAPk(Context context,String path)
		{
			installAPk(context,new File(path));
		}
		/**
		 * 安装应用程序的方法
		 * @param context	上下文
		 * @param file		应用程度file对象
		 */
		public static void installAPk(Context context,File file)
		{
			if (!file.exists() || file.length()<=0)
			{
				throw new RuntimeException("File Not Found Exception");
			}
			Intent intent = new Intent();
			intent.setAction("android.intent.action.VIEW");
			intent.addCategory("android.intent.category.DEFAULT");
			intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
			context.startActivity(intent);
		}
		
		/**
		 * 获取应用程序的版本号
		 * @param context	上下文
		 * @return			应用程序的版本号
		 */
		public static String getVersionName(Context context)
		{
			try
			{
				PackageManager packageManager = context.getPackageManager();
				PackageInfo packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
				String version = packInfo.versionName;
				return version;
			} catch (NameNotFoundException e)
			{
				e.printStackTrace();
				return null;
			}
		}
		
	}
}
