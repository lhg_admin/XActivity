package com.xcode.util;

import android.app.ActivityManager;
import android.content.Context;

public class ActivityManagerUtil
{
	/**
	 * 将自己应用程序完全退出进程的方法
	 */
	public static void KillApplicationSelfe()
	{
		android.os.Process.killProcess(android.os.Process.myPid());
	}
	/**
	 * 将自己应用程序完全退出进程的方法<br>
	 * 但是必须在manifest中添加权限<br>
	 * <b>android.permission.KILL_BACKGROUND_PROCESSES</b>
	 * @param context		应用程序的上下文
	 * @param packageName	需要杀死的应用程序报名
	 */
	public static void KillOtherApplication(Context context,String packageName)
	{
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		activityManager.killBackgroundProcesses(packageName);
	}
	
}
