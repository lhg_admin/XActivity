package com.xcode.util;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.xcode.bean.NetResult;

/**
 * 对访问网络的操作进行一些封装
 * @author 肖蕾
 */
public class NetworkUtil
{
	/**
	 * 通过Get的方式向服务器发送请求 并且返回数据
	 * @param activity  activity界面
	 * @param url 		请求的链接
	 * @param result 	请求的结果对象
	 */
	public static void Get(final Activity activity,final String url, final NetResult result)
	{
		final HttpClient client = new DefaultHttpClient();
		new Thread()
		{
			@Override
			public void run()
			{
				// 2.输入地址
				HttpGet get = new HttpGet(url);
				// 3.敲下回车
				HttpResponse response = null;
				try
				{
					response = client.execute(get);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
				StatusLine statusLine = response.getStatusLine();// 获取状态行
				final int code = statusLine.getStatusCode();// 获取状态码
				if (code == 200)
				{
					final StringBuffer sBuffer = new StringBuffer();
					try
					{
						InputStream inputStream = response.getEntity().getContent();
						byte []data = new byte[1024];
						int len = 0;
						while ((len = inputStream.read(data)) != -1)
						{
							sBuffer.append(new String(data,0,len));
						}
						inputStream.close();
					} catch (Exception e)
					{
						e.printStackTrace();
					}
					activity.runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							result.success(sBuffer.toString());
						}
					});
				} else
				{
					activity.runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							result.failed(code);
						}
					});
				}
			};
		}.start();
	}
	
	/**
	 * 用于下载文件的函数
	 * @param activity  activity 界面
	 * @param url		下载链接
	 * @param result	请求网络的结果以便做不同的处理
	 */
	public static void download(final Activity activity,final String url,final NetResult result)
	{
		final HttpClient client = new DefaultHttpClient();
		new Thread()
		{
			@Override
			public void run()
			{
				// 2.输入地址
				HttpGet get = new HttpGet(url);
				// 3.敲下回车
				HttpResponse response = null;
				try
				{
					response = client.execute(get);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
				StatusLine statusLine = response.getStatusLine();// 获取状态行
				final int code = statusLine.getStatusCode();// 获取状态码
				if (code == 200)
				{
					try
					{
						InputStream inputStream = response.getEntity().getContent();
						result.success(inputStream);
						inputStream.close();
					} catch (IllegalStateException e)
					{
						/**
						 * 此处异常没事，
						 * 是inputstream在result.success(inputStream)中被手动关闭了输入流
						 * 此处再次关闭造成的，并不影响程序的运行
						 */
					} catch (IOException e)
					{
						e.printStackTrace();
					}
					
				} else
				{
					activity.runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							result.failed(code);
						}
					});
				}
			};
		}.start();
	
	}
	
	/**
	 * 通过Post方式往服务器提交数据并获得返回数据
	 * @param activity  界面的activity
	 * @param url 		访问的URL
	 * @param params 	需要传递的参数
	 * @param result 	代表请求结果的对象
	 */
	public static void Post(final Activity activity,final String url, final Map<String, String> params, final NetResult result)
	{
		final HttpClient client = new DefaultHttpClient();
		new Thread()
		{
			@Override
			public void run()
			{
				// 2.输入地址
				HttpPost post = new HttpPost(url);
				HttpClientParams.setCookiePolicy(client.getParams(), CookiePolicy.BROWSER_COMPATIBILITY);
				List<NameValuePair> parameters = new ArrayList<NameValuePair>();
				for (Map.Entry<String, String> entry : params.entrySet())
				{
					parameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
				}
				try
				{
					post.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
				} catch (UnsupportedEncodingException e1)
				{
					e1.printStackTrace();
				}// 指定要提交的数据实体
					// 3.敲下回车
				HttpResponse response = null;
				try
				{
					response = client.execute(post);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
				StatusLine statusLine = response.getStatusLine();// 获取状态行
				final int code = statusLine.getStatusCode();// 获取状态码
				if (code == 200)
				{
					final StringBuffer sBuffer = new StringBuffer();
					try
					{
						byte []data = new byte[1024];
						int len = 0;
						InputStream inputStream = response.getEntity().getContent();
						while ((len = inputStream.read(data)) != -1)
						{
							sBuffer.append(new String(data,0,len));
						}
						inputStream.close();
					} catch (Exception e1)
					{
						e1.printStackTrace();
					}
					activity.runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							result.success(sBuffer.toString());
						}
					});
				} else
				{
					activity.runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							result.failed(code);
						}
					});
				}
			};
		}.start();
	}
	
	/**
	 * 通过Post方式上传文件以及提交表单
	 * @param activity   	界面的activity
	 * @param actionUrl		为要使用的URL
	 * @param params		为表单内容	name_value
	 * @param files			参数为要上传的文件，可以上传多个文件 name_file
	 */
	public static void Post(final Activity activity,final String actionUrl, final Map<String, String> params,final Map<String, File> files,final NetResult netResult)
	{
		new Thread()
		{
			@Override
			public void run()
			{
				try
				{
					String BOUNDARY = java.util.UUID.randomUUID().toString();
					String PREFIX = "--", LINEND = "\r\n";
					String MULTIPART_FROM_DATA = "multipart/form-data";
					String CHARSET = "UTF-8";
					URL uri = new URL(actionUrl);
					HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
					conn.setReadTimeout(5 * 1000);
					conn.setDoInput(true);// 允许输入
					conn.setDoOutput(true);// 允许输出
					conn.setUseCaches(false);
					conn.setRequestMethod("POST"); // Post方式
					conn.setRequestProperty("connection", "keep-alive");
					conn.setRequestProperty("Charsert", "UTF-8");
		
					conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA+ ";boundary=" + BOUNDARY);
		
					// 首先组拼文本类型的参数
					StringBuilder sb = new StringBuilder();
					for (Map.Entry<String, String> entry : params.entrySet())
					{
						sb.append(PREFIX);
						sb.append(BOUNDARY);
						sb.append(LINEND);
						sb.append("Content-Disposition: form-data; name=\""+ entry.getKey() + "\"" + LINEND);
						sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
						sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
						sb.append(LINEND);
						sb.append(entry.getValue());
						sb.append(LINEND);
					}
		
					DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
					outStream.write(sb.toString().getBytes());
					// 发送文件数据
					if (files != null)
						for (Map.Entry<String, File> file : files.entrySet())
						{
							StringBuilder sb1 = new StringBuilder();
							sb1.append(PREFIX);
							sb1.append(BOUNDARY);
							sb1.append(LINEND);
							sb1.append("Content-Disposition: form-data; name=\""+file.getKey()+"\"; filename=\""+ file.getValue().getName() + "\"" + LINEND);
							sb1.append("Content-Type: application/octet-stream; charset="+ CHARSET + LINEND);
							sb1.append(LINEND);
							outStream.write(sb1.toString().getBytes());
							InputStream is = new FileInputStream(file.getValue());
							byte[] buffer = new byte[1024];
							int len = 0;
							while ((len = is.read(buffer)) != -1)
							{
								outStream.write(buffer, 0, len);
							}
							is.close();
							outStream.write(LINEND.getBytes());
						}
		
					// 请求结束标志
					byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
					outStream.write(end_data);
					outStream.flush();
					// 得到响应码
					final int responseCode = conn.getResponseCode();
					final InputStream inputStream = conn.getInputStream();
					if (responseCode == 200)
					{
						activity.runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								byte []data = new byte[1024];
								int len = 0;
								StringBuffer buffer = new StringBuffer();
								try
								{
									while ((len = inputStream.read(data)) != -1)
									{
										buffer.append(new String(data,0,len));
									}
									netResult.success(buffer.toString());
								} catch (IOException e)
								{
									e.printStackTrace();
								}
							}
						});
					}else 
					{
						activity.runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								netResult.failed(responseCode);
							}
						});
					}
					outStream.close();
					conn.disconnect();
				} catch (Exception e)
				{
					
				}
			}
		}.start();
	}
	/**
	 * 检测网络状态是否可用
	 * @param context 上下文
	 * @return	如果可用则返回true 否则false
	 */
	public static boolean isNetWorkUseful(Context context)
	{
		ConnectivityManager cM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cM.getActiveNetworkInfo();
		return (info != null&&info.isAvailable())?true:false;
	}
}
