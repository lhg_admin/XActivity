package com.xcode.util;

import android.app.Activity;

public class WindowUtil
{
	/**
	 * 获取手机宽度
	 * @return 手机屏幕宽度
	 */
	@SuppressWarnings("deprecation")
	public static int getWindowWindth(Activity context)
	{
		return context.getWindowManager().getDefaultDisplay().getWidth();
	}
	
	/**
	 * 获取手机屏幕高度
	 * @return	手机屏幕高度
	 */
	@SuppressWarnings("deprecation")
	public static int getWindowHeight(Activity context)
	{
		return context.getWindowManager().getDefaultDisplay().getHeight();
	}
	
}
