package com.xcode.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 对系统的 android.content.SharedPreferences 进行一个工具类的封装
 * @author 肖蕾
 */
public class sharedPreferences
{
	final static String sharedPreferencesName = "sharedPreferences"; 
	/**
	 * 从SharedPreference里面获取值
	 * @param context 	上下文
	 * @param key		存在SharedPreference里面值的key
	 * @return			获取到的value 如果没有  则返回null空对象
	 */
	public static String getSharedPreferenceValue(Context context,String key)
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences( sharedPreferencesName, Context.MODE_PRIVATE);
		return sharedPreferences.getString(key, null);
	}
	/**
	 * 从指定的SharedPreference里面获取值
	 * @param sharedPreferences 	指定的SharedPreference
	 * @param key					存在SharedPreference里面值的key
	 * @return						获取到的value 如果没有  则返回null空对象
	 */
	public static String getSharedPreferenceValue(SharedPreferences sharedPreferences,String key)
	{
		return sharedPreferences.getString(key, null);
	}
	/**
	 * 往sharedPreferences里面存储数据
	 * @param context 		上下文
	 * @param key 		 	key
	 * @param value     	值
	 * @return 				存入成功则返回true 否则返回false
	 */
	public static boolean PutSharedPreferences(Context context,String key, Object value)
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences( sharedPreferencesName, Context.MODE_PRIVATE);
			
		return PutSharedPreferences(sharedPreferences,key,value);
	}
	/**
	 * 往指定的sharedPreferences里面存储数据
	 * 
	 * @param sharedPreferences 	指定的sharedPreferences对象
	 * @param key 					存储的key
	 * @param value 				存储的值
	 * @return 						存储成功返回true 否则返回false
	 */
	public static boolean PutSharedPreferences(SharedPreferences sharedPreferences, String key, Object value)
	{
		try
		{
			Editor editor = sharedPreferences.edit();
	
			if (value instanceof String || value instanceof CharSequence)
			{
				editor.putString(key, (String) value);
			} else if (value instanceof Boolean)
			{
				editor.putBoolean(key, (Boolean) value);
			} else if (value instanceof Float)
			{
				editor.putFloat(key, (Float) value);
			}else if (value instanceof Double)
			{
				editor.putFloat(key, ((Double) value).floatValue());
			}else if (value instanceof Character) 
			{
				editor.putInt(key, ((Character) value).charValue());
			}else if (value instanceof Short) 
			{
				editor.putInt(key, ((Short) value).intValue());
			}else if (value instanceof Byte) 
			{
				editor.putInt(key, ((Byte) value).intValue());
			}else if (value instanceof Integer)
			{
				editor.putInt(key, (Integer) value);
			} else if (value instanceof Long)
			{
				editor.putLong(key, (Long) value);
			}else 
			{
				return false;
			}
			editor.commit();
			return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * 获取默认的SharedPreferences对象
	 * @param context	上下文
	 * @return	返回SharedPreferences对象
	 */
	public static SharedPreferences GetDefaultSharedPreferences(Context context)
	{
		return context.getSharedPreferences( sharedPreferencesName, Context.MODE_PRIVATE);
	}
	
	/**
	 * 获取自己设置的名称的SharedPreferences对象
	 * @param context 上下文
	 * @param SharedPreferenceName 	SharedPreference文件名字
	 * @return	返回自己设置的名称的SharedPreferences对象
	 */
	public static SharedPreferences GetSharedPreferences(Context context,String SharedPreferenceName)
	{
		return context.getSharedPreferences( SharedPreferenceName, Context.MODE_PRIVATE);
	}
}
