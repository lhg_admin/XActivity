package com.xcode.util;

import android.content.Context;
import android.view.View;
/**
 * 对系统的android.widget.Toast进行简单的封装
 * @author 肖蕾
 *
 */
public class Toast
{
	/**
	 * 显示提示信息
	 * 
	 * @param string 提示信息
	 */
	public static void Show(Context context,String string)
	{
		android.widget.Toast.makeText(context, string, android.widget.Toast.LENGTH_SHORT).show();
	}
	/**
	 * 显示自定义提示信息
	 * 
	 * @param layout_ID 自定义提示信息的id
	 */
	public static void Show(Context context,int layout_ID)
	{
		View view = View.inflate(context, layout_ID, null);
		android.widget.Toast toast = new android.widget.Toast(context);
		toast.setView(view);
		toast.setDuration(1000);
		toast.show();
	}
	/**
	 * 显示自定义提示信息
	 * 
	 * @param view 自定义提示信息的view对象
	 */
	public static void Show(Context context,View view)
	{
		android.widget.Toast toast = new android.widget.Toast(context);
		toast.setView(view);
		toast.setDuration(1000);
		toast.show();
	}
}
