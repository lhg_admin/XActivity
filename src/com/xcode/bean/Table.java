package com.xcode.bean;



import java.util.HashMap;
import java.util.Map;
/**
 * 自定义的表
 * 
 * @author 肖蕾
 */
public class Table
{

	public class paramType
	{
		public static final String INTEGER = "INTEGER";
		public static final String TEXT = "TEXT";
		public static final String REAL = "REAL";
		public static final String BLOB = "BLOB";
		public static final String INTEGER_NotNull = "INTEGER NOT NULL";
		public static final String TEXT_NotNull = "TEXT NOT NULL";
		public static final String REAL_NotNull = "REAL NOT NULL";
		public static final String BLOB_NotNull = "BLOB NOT NULL";
	}

	public HashMap<String, String> tabMap;
	public String keyName;
	public String keyType;
	public String tabName;

	public Table(String TableName)
	{
		this.tabName = TableName;
		tabMap = new HashMap<String, String>();
	}

	public Table(String TableName, String keyName, String keyType)
	{
		this.tabName = TableName;
		this.keyName = keyName;
		this.keyType = keyType;
		tabMap = new HashMap<String, String>();
	}

	/**
	 * 设置主键，设置字段的名字，类型。以及是否自增长....等等属性
	 * 
	 * @param Name
	 *            字段名字
	 * @param type
	 *            字段类型
	 * @return Table
	 */
	public Table setKey(String Name, String type)
	{
		this.keyName = Name;
		this.keyType = type;
		return this;
	}

	/**
	 * 添加字段 字段名，类型
	 * 
	 * @param Name
	 *            字段名
	 * @param Type
	 *            字段类型
	 * @return Table
	 */
	public Table addParam(String Name, String Type)
	{
		tabMap.put(Name, Type);
		return this;
	}

	/**
	 * 为表添加字段
	 * 
	 * @param params
	 *            字段名与字段类型保存在Map里
	 * @return Table
	 */
	public Table addParam(Map<String, String> params)
	{
		tabMap.putAll(params);
		return this;
	}

}
