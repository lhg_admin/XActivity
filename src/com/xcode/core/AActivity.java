package com.xcode.core;

import java.lang.reflect.Field;
import android.os.Bundle;
import com.xcode.annotation.findViewById;
import com.xcode.annotation.setContentView;

/**
 * 继承此类可以简化许多操作，初始化
 * @author 肖蕾
 */
public abstract class AActivity extends XActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(getContentViewId());
		AnnotationInit();
		Init();
	}
	/**
	 * 首先通过此方法 获取到contentviewid
	 * @return  获取到contentviewid
	 */
	private int getContentViewId()
	{
		Class<?> class1 = this.getClass();
		/**
		 * 判断是否是这个注解 注解的类
		 */
		if (class1.isAnnotationPresent(setContentView.class))
		{
			setContentView ContentView = class1.getAnnotation(setContentView.class);
			return  ContentView.value();
		}else 
		{
			return 0;
		}
	}
	/**
	 * 对注解的View对象进行初始化
	 */
	private void AnnotationInit()
	{
		Class<?> class1 = this.getClass();
		Field[] fields = class1.getDeclaredFields();//获得Activity中声明的字段
		for(Field field:fields)
		{
			// 查看这个字段是否有我们自定义的注解类标志的
			if (field.isAnnotationPresent(findViewById.class)) 
			{
				findViewById inject = field.getAnnotation(findViewById.class);
				int id = inject.value();
				if (id > 0) 
				{
					try
					{
						field.setAccessible(true);
						//给我们要找的字段设置值
						field.set(this, this.findViewById(id));
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}
	@Override
	protected void onStart()
	{
		super.onStart();
		OnStart();
	}
	public abstract void Init();
	public abstract void OnStart();
}
